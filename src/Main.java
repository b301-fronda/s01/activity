import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // variable declaration
        String firstName;
        String lastName;
        double firstSubject;
        double secondSubject;
        double thirdSubject;
        int average;

        Scanner obj = new Scanner(System.in); // Create a new scanner object

        System.out.println("First Name: ");
        firstName = obj.nextLine();

        System.out.println("Last Name: ");
        lastName = obj.nextLine();

        System.out.println("First Subject Grade: ");
        firstSubject = obj.nextDouble();

        System.out.println("Second Subject Grade: ");
        secondSubject = obj.nextDouble();

        System.out.println("Third Subject Grade: ");
        thirdSubject = obj.nextDouble();

        // Find the average grade
        average = ((int)(firstSubject + secondSubject + thirdSubject)) / 3;

        System.out.println("Good day, " + firstName + " " + lastName + ".\nYour average grade is: " + average);
    }
}